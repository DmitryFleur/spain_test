from rest_framework.views import APIView
from rest_framework.response import Response
from music.models import Song, Contributors


class SongDetailsApiView(APIView):

    def get(self, request, *args, **kwargs):
        iswc = kwargs.get('iswc', None)

        try:
            song = Song.objects.get(iswc=iswc)
        except Song.DoesNotExist:
            return Response({'msg': 'Song with iswc "%s" was not found in the db' % iswc, 'err': True})
        else:
            contributors = Contributors.objects.filter(song=song.id)
            song_data = {'iswc': song.iswc,
                         'title': song.title,
                         'contributors': [str(obj) for obj in contributors]}

            return Response({'msg': song_data, 'err': False})
