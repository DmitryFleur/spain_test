from django.urls import include, path
from rest_framework import routers
from api.views import SongDetailsApiView

router = routers.DefaultRouter()

urlpatterns = [
    path('song/<str:iswc>/', SongDetailsApiView.as_view())
]
