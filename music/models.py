from django.db import models


class Song(models.Model):
    title = models.CharField(max_length=300)
    iswc = models.CharField(max_length=15, db_index=True)

    def __str__(self):
        return self.title


class Contributors(models.Model):
    song = models.ForeignKey(Song, on_delete=models.CASCADE)
    full_name = models.CharField(max_length=300)

    def __str__(self):
        return self.full_name
