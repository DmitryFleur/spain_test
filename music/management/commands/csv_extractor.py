import csv
from django.core.management.base import BaseCommand
from music.models import Song, Contributors


class Command(BaseCommand):
    help = 'Data Population from CSV file'

    def add_arguments(self, parser):
        parser.add_argument('file_path',
                            type=str, help='CSV file path')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']
        self.stdout.write('Starting importing from  %s' % file_path)

        with open(file_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            next(csv_reader, None)

            for row in csv_reader:
                title = row[0]
                contributors = row[1].split('|')
                iswc = row[2]

                try:
                    song = Song.objects.get(title=title)
                except Song.DoesNotExist:
                    song = Song.objects.create(title=title, iswc=iswc)
                else:
                    if not song.iswc:
                        contribs_in_db = Contributors.objects.filter(full_name__in=contributors,
                                                                     song__title=title)
                        if contribs_in_db:
                            song.iswc = iswc
                            song.save()
                self.create_contributors(song, contributors)

        self.stdout.write('Import has been finished')

    @staticmethod
    def create_contributors(song, contributors):
        for contributor in contributors:
            Contributors.objects.get_or_create(song=song, full_name=contributor)
