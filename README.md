INSTRUCTIONS

1. Installing dependencies with `pip install -r requirements.txt`
As we are using `elephantsql.com` service, all the migrations are already done there, so we do not need `python manage.py migrate` etc
2. `python manage.py runserver`
3. Now you can query our API with `http://127.0.0.1:8000/api/song/<song_iswc>/`. For example: `http://127.0.0.1:8000/api/song/T0101974597/`

In order to run Django command and to populate db from the file (1 task), we should run following:
`python manage.py csv_extractor <path_to_csv_file>`


ANSWERS

1.1.
Trying to get a song from the db by title. If nothing was found, we are creating a new record, otherwise
in order to make sure that the song is the one that we were looking for we are verifying contributors
in db and in csv file (as it was it a test assignment: title and at least one contributor should be the same).
If it is the right song, we are doing get_or_create on contributors.

1.2
I think it depends in what format they are sending data to you.
- Some api endpoint, where providers will be able to upload new csv files.
- In case providers have api where they are frequently updating their data,
  we can do some kind of Celery task that will be sending requests to that endpoint from time to time.
- If they are providing data with csv files, we can scan local directory for a csv files and update record in db.

2.1
No

2.2
As we are querying our api with 'iswc' code and it is unique, seems like a good point would be to add index to that field.
Also as far as I know, when operating big amount of data, Elastic search would be a really handy.